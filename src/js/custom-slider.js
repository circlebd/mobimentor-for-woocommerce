'use strict';
(function ($) {
	jQuery(window).on('elementor/frontend/init', function(){
        elementorFrontend.hooks.addAction('frontend/element_ready/modimentor_woocommerce_product_carousel.default', function ($scope, $) {
            var sliderSettings = $scope.find('.slider-settings')
            var slider = $scope.find('.mobimentor-slick-slider')
            sliderSettings = JSON.parse(sliderSettings.val())
            slider.not('.slick-initialized').slick({
                speed: sliderSettings.slide_speed,
                autoplay: sliderSettings.autoplay,
                autoplaySpeed: sliderSettings.autoplay_speed,
                slidesToShow: sliderSettings.slides_to_show,
                slidesToScroll: sliderSettings.slides_to_scroll,
                variableWidth: false,
                arrows: sliderSettings.display_nav_arrows,
                prevArrow: '<div class="slick-arrow-left"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>',
                nextArrow: '<div class="slick-arrow-right"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>',
                dots: sliderSettings.display_dots,
                pauseOnFocus: sliderSettings.pause_on_focus,
                pauseOnHover: sliderSettings.pause_on_hover,
                responsive: [
                    {
                    breakpoint: 769,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 2,
                        swipeToSlide: true
                    }
                    },
                    {
                    breakpoint: 482,
                    settings: {
                        arrows: false,
                        centerMode: false,
                        centerPadding: '40px',
                        slidesToShow: 1,
                        swipeToSlide: true
                    }
                    }
                ]
            });
        })
	});
})(jQuery)