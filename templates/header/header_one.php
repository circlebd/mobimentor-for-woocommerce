<?php

/* Mobimentor Title One */

?>
<div class="mobimentor-widget-title <?php echo $title_style; ?>">
    <div class="mobimentor-title-left">
        <?php if ($settings['display_title'] == 'yes') { ?>
            <h2 class="mobimentor-title"><?php echo $settings['title']; ?></h2>
        <?php } ?>
        <?php if ($settings['display_subtitle'] == 'yes') { ?>
            <h3 class="mobimentor-subtitle"><?php echo $settings['subtitle']; ?></h3>
        <?php } ?>
    </div>
    <div class="mobimentor-title-right">
        <?php if ($settings['show_more'] == 'yes') { ?>
            <a class="mobimentor-showmore" href="<?php echo $settings['show_more_link']; ?>" target="_blank">
                <?php echo $settings['show_more_text']; ?>
            </a>
        <?php } ?>
    </div>
</div>