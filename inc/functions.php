<?php

/* Mobimentor Get Carousel Settings */

if (!function_exists('mobimentor_get_carousel_settings')) {
    function mobimentor_get_carousel_settings($settings)
    {
        $slider_settings = array();
        $slider_settings['carousel_height'] = $settings['carousel_height'];
        $slider_settings['slides_to_show'] = $settings['slides_to_show'];
        $slider_settings['slides_to_scroll'] = $settings['slides_to_scroll'];
        $slider_settings['pause_on_focus'] = $settings['pause_on_focus'] == 'yes' ? true : false;
        $slider_settings['display_nav_arrows'] = $settings['display_nav_arrows'] == 'yes' ? true : false;
        $slider_settings['display_dots'] = $settings['display_dots'] == 'yes' ? true : false;
        $slider_settings['autoplay'] = $settings['autoplay'] == 'yes' ? true : false;
        $slider_settings['autoplay_speed'] = $settings['autoplay_speed'];
        $slider_settings['pause_on_hover'] = $settings['pause_on_hover'] == 'yes' ? true : false;
        $slider_settings['pause_dots_hover'] = $settings['pause_dots_hover'] == 'yes' ? true : false;
        $slider_settings['slide_speed'] = $settings['slide_speed'];
        return $slider_settings;
    }
}

/* Mobimentor Get Product Status */

if (!function_exists('mobimentor_get_product_status')) {
    function mobimentor_get_product_status()
    {
        $post_statuses = array();
        $post_statuses['any'] = esc_html__('Any', 'wpce`');
        $post_statuses = get_post_statuses();
        return $post_statuses;
    }
}


/* Mobimentor Get Product Types */

if (!function_exists('mobimentor_get_product_types')) {
    function mobimentor_get_product_types()
    {
        $product_types_lists = wc_get_product_types();
        return $product_types_lists;
    }
}

/* Mobimentor Get Product Categories  */

if (!function_exists('mobimentor_get_product_cats')) {
    function mobimentor_get_product_cats($category = 'product_cat')
    {
        $product_categories_list = array();
        $args = array(
            'taxonomy'   => $category,
        );
        $args = apply_filters('mobimentor_get_product_cat_args', $args);
        $product_categories = get_terms($args);

        if (!empty($product_categories)) {
            foreach ($product_categories as $cat) {
                $product_categories_list[$cat->slug] = $cat->name;
            }
        }
        return $product_categories_list;
    }
}

/* Mobimentor Get Slider Query */

if (!function_exists('mobimnetor_get_slider_query')) {
    function mobimentor_get_slider_query($settings)
    {
        $template = $settings['template_style'];
        $limit = $settings['product_limit'];
        $status = $settings['product_status'];
        $types = $settings['product_types'];
        $cats = $settings['product_cats'];
        $options = $settings['product_options'];
        $sorting_by = $settings['product_sorting_by'];
        $sorting_order = $settings['product_sorting_order'];

        $meta_query = $tax_query = [];

        if ($template == 'template-one') {
            $meta_query[] = array(
                'key' => '_thumbnail_id',
                'compare' => 'EXISTS'
            );
        }
        if ($options == 'featured') {
            $tax_query[] = array(
                'taxonomy' => 'product_visibility',
                'field'    => 'name',
                'terms'    => 'featured',
                'operator' => 'IN', // or 'NOT IN' to exclude feature products
            );
        }
        if (!empty($types)) {
            $tax_query[] = array(
                'taxonomy' => 'product_type',
                'field'    => 'slug',
                'terms'    => $types,
                'operator' => 'IN',
            );
        }
        if (!empty($cats)) {
            $tax_query[] = array(
                'taxonomy' => 'product_cat',
                'field'    => 'slug',
                'terms'    => $cats,
                'operator' => 'IN',
            );
        }

        $order_by = 'title';
        switch ($sorting_by) {
            case 'name':
                $order_by = 'title';
                break;
            case 'date':
                $order_by = 'date';
                break;
            case 'price':
                $order_by = 'meta_value_num';
                break;
        }

        $args = array(
            'post_type' => 'product',
            'post_status' => $status,
            'posts_per_page' => $limit ? $limit : 12,
            'meta_query' => $meta_query,
            'tax_query' => $tax_query,
            'order' => $sorting_order,
            'orderby' => $order_by,
        );

        if ($sorting_by == 'price') {
            $args['meta_key'] = '_price';
        }

        return $args;
    }
}


/* Mobimentor Image Size / Ratio */

if (!function_exists('mobimentor_image_ratio')) {
    function mobimentor_image_ratio($image_src = '', $settings)
    {
        if (!empty($image_src)) {
            $carousel_height = $settings['carousel_height'];
            $slides_to_show = $settings['slides_to_show'];
            $size = getimagesize($image_src);
            $height = $size[1];
            $width = $size[0];
            if ($carousel_height < $height && $slides_to_show < 2) {
                $image_size = 'width: 100%';
            } else {
                $image_size = $height > $width ? 'width: 100%' : 'height: 100%';
            }
            return $image_size;
        }
    }
}
