// Gulp.js configuration
'use strict';
const

  // source and build folders
  dir = {
    src         : 'src/',
    build       : 'dist/'
  },

  // Gulp and plugins
  gulp          = require('gulp'),
  gutil         = require('gulp-util'),
  newer         = require('gulp-newer'),
  imagemin      = require('gulp-imagemin'),
  sass          = require('gulp-sass'),
  postcss       = require('gulp-postcss'),
  deporder      = require('gulp-deporder'),
  concat        = require('gulp-concat'),
  stripdebug    = require('gulp-strip-debug'),
  uglify        = require('gulp-uglify'),
  plumber       = require('gulp-plumber'),
  autoprefixer  = require('autoprefixer'),
  cleanCSS      = require('gulp-clean-css'),
  rename        = require('gulp-rename'),
  babel         = require('gulp-babel'),
  sourcemaps    = require('gulp-sourcemaps')
;
// Browser-sync
var browsersync = false;
// PHP settings
const php = {
  src           : dir.src + 'template/**/*.php',
  build         : dir.build + 'templates'
};
// copy PHP files
gulp.task('php', () => {
  return gulp.src(php.src)
    .pipe(newer(php.build))
    .pipe(gulp.dest(php.build));
});

// image settings
const images = {
  src         : dir.src + 'images/**/*',
  build       : dir.build + 'images/'
};

// image processing
gulp.task('images', () => {
  return gulp.src(images.src)
    .pipe(newer(images.build))
    .pipe(imagemin())
    .pipe(gulp.dest(images.build));
});

// Run:
// gulp sass
// Compiles SCSS files in CSS
gulp.task('sass', function() {
	var stream = gulp
		.src(dir.src + '/scss/*.scss')
		.pipe(
			plumber({
				errorHandler: function(err) {
					console.log(err);
					this.emit('end');
				}
			})
		)
		.pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(sass({ errLogToConsole: true }))
		.pipe(postcss([autoprefixer()]))
		.pipe(sourcemaps.write(undefined, { sourceRoot: null }))
		.pipe(gulp.dest(dir.build+'/css'));
	return stream;
});

gulp.task('minifycss', function() {
	return gulp
		.src(dir.build+'/css/style.css')
		.pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(cleanCSS({ compatibility: '*' }))
		.pipe(
			plumber({
				errorHandler: function(err) {
					console.log(err);
					this.emit('end');
				}
			})
		)
		.pipe(rename({ suffix: '.min' }))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(dir.build+'/css'));
});

gulp.task('styles', function(callback) {
	gulp.series('sass', 'minifycss')(callback);
});

gulp.task('scripts', function() {
	var scripts = [     
		dir.src + 'js/custom-slider.js',
	];
	gulp
		.src(scripts, { allowEmpty: true })
		.pipe(babel(
			{
				presets: ['@babel/preset-env']
			}
		))
		.pipe(concat('mobimentor.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest(dir.build + '/js'));

	return gulp
		.src(scripts, { allowEmpty: true })
		.pipe(babel())
		.pipe(concat('mobimentor.js'))
		.pipe(gulp.dest(dir.build + '/js'));
});

gulp.task('watch', function() {
	gulp.watch( dir.src + 'scss/**/*.scss', gulp.series('styles') );
    gulp.watch( dir.src + 'js/**/*.js', gulp.series('scripts') );
});